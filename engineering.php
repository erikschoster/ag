<?php
/**
 * Template Name: Engineering 
 */
?>

<?php get_header(); ?>
<?php while(have_posts()): the_post(); ?>

<div id="breadcrumbs"></div>

<div class="engineering-wrapper" id="project-landing-content">
    <h2 id="engineering-header"><?php the_title(); ?></h2>

    <div id="our-work-content-wrapper">

        <div class="our-work-content" id="engineering-content">
            <h3>Engineering Services</h3>
            <?php the_content(); ?>
        </div>

        <div id="engineering-thumbs">
            <div class="engineering-thumb" id="thumb-structural">
                <h3>Structural</h3>
                <img src="/wp-content/themes/ag/img/engineering-structural-thumb.jpg" />

                <ul>
            <li> AG Engineering has the capabilities to analyze existing structures and provide preliminary analysis for cost efficiency. We provide the complete design of wood, steel, concrete, and masonry structural systems as well as the design of both shallow and deep foundation systems.</li>
                </ul>
            </div>

            <div class="engineering-thumb">
                <h3>Mechanical</h3>
                <img src="/wp-content/themes/ag/img/engineering-mechanical-thumb.jpg" />

                <ul>
                    <li>Analysis of existing mechanical equipment and layout allows AG to offer solutions in the best interest of its clients. Our forced air and hydronic HVAC system designs include products and systems that provide the best comfort while considering long term operating and maintenance concerns.</li>
                </ul>
            </div>

            <div class="engineering-thumb">
                <h3>Electrical</h3>
                <img src="/wp-content/uploads/2012/06/ag_electric_engineering.jpg" />

                <ul>
                    <li>Building electrical systems are becoming increasingly complex. Not only does AG design lighting and power distribution, we address life safety and emergency power requirements. We work with a number of vendors to incorporate low voltage, networking, fire alarm and security systems.</li>
                </ul>
            </div>

            <div class="engineering-thumb" id="thumb-plumbing">
                <h3>Plumbing</h3>
                <img src="/wp-content/themes/ag/img/engineering-plumbing-thumb.jpg" />

                <ul>
                    <li>The AG Engineering team offers full service plumbing capabilities. We design domestic water, waste, and storm conveyance systems as well as kitchen grease and other special waste systems. In addition, we coordinate with water, sewer, and natural gas utilities to ensure the site is adequately served.  Additionally, we specify and review fire sprinkler systems.</li>
                </ul>
            </div>
        </div>

    </div>
</div>

<div id="upper-footer">
    <?php get_template_part('footer_collab'); ?>
    <?php get_template_part('footer_experience'); ?>
    <?php get_template_part('footer_news'); ?>
</div>

<?php endwhile; ?>
<?php get_footer(); ?>
