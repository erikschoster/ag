<div id="footer">
    <a href="/"><img src="/wp-content/themes/ag/img/ag-footer-logo-color.png" id="footer-logo" /></a>
    <div id="footer-copyright">
        <p>Copyright &copy; 2012 AG Architecture. All rights reserved.</p>
        <p><a href="/">Home</a> &bull; <a href="/about-us/">About Us</a> &bull; <a href="/employment/">Employment</a> &bull; <a href="/contact-us/contact-form/">Contact Us</a></p>
    </div>
</div>

<?php wp_footer(); ?>
</div> <!-- #main-wrapper -->
</body>
</html>
