<?php

class HeyAG {
    public function get_project_category($id) {
        $cats = get_the_category($id);

        foreach($cats as $cat):
            if($cat->category_parent == 0):
                return array(
                        'slug' => $cat->slug, 
                        'name' => $cat->name,
                        'id' => $cat->cat_ID,
                       );
            endif;
        endforeach;

        return false;
    }

    public function get_project_meta($id, $meta = array()) {
        $meta['details'] = get_post_meta($id, 'hey_project_details', true);
        $meta['description'] = get_post_meta($id, 'hey_project_description', true);
        $meta['quote'] = get_post_meta($id, 'hey_project_quote', true);
        $meta['attribution'] = get_post_meta($id, 'hey_project_quote-attribution', true);
        $meta['case_study'] = get_post_meta($id, 'hey_project_case_study', true);

        return preg_replace('/\n/', '<br/>', $meta);
    }

    public function get_leader_meta($id, $meta = array()) {
        $meta['bio'] = get_post_meta($id, 'hey_leader_bio', true);
        $meta['job_title'] = get_post_meta($id, 'hey_leader_job_title', true);

        $meta['experience'] = get_post_meta($id, 'hey_leader_experience', true);
        $meta['project_position'] = get_post_meta($id, 'hey_leader_project_position', true);
        $meta['expertise'] = get_post_meta($id, 'hey_leader_expertise', true);
        $meta['education'] = get_post_meta($id, 'hey_leader_education', true);
        $meta['project'] = get_post_meta($id, 'hey_leader_project', true);

        $meta['quote'] = get_post_meta($id, 'hey_leader_quote', true);
        $meta['attribution'] = get_post_meta($id, 'hey_leader_attribution', true);

        return preg_replace('/\n/', '<br/>', $meta);
    }

    public function get_associate_meta($id, $meta = array()) {
        $meta['bio'] = get_post_meta($id, 'hey_associate_bio', true);
        $meta['job_title'] = get_post_meta($id, 'hey_associate_job_title', true);

        $meta['experience'] = get_post_meta($id, 'hey_associate_experience', true);
        $meta['project_position'] = get_post_meta($id, 'hey_associate_project_position', true);
        $meta['expertise'] = get_post_meta($id, 'hey_associate_expertise', true);
        $meta['education'] = get_post_meta($id, 'hey_associate_education', true);
        $meta['project'] = get_post_meta($id, 'hey_associate_project', true);

        $meta['quote'] = get_post_meta($id, 'hey_associate_quote', true);
        $meta['attribution'] = get_post_meta($id, 'hey_associate_attribution', true);

        return preg_replace('/\n/', '<br/>', $meta);
    }

    public function get_support_meta($id, $meta = array()) {
        $meta['bio'] = get_post_meta($id, 'hey_support_bio', true);
        $meta['job_title'] = get_post_meta($id, 'hey_support_job_title', true);

        $meta['experience'] = get_post_meta($id, 'hey_support_experience', true);
        $meta['project_position'] = get_post_meta($id, 'hey_support_project_position', true);
        $meta['expertise'] = get_post_meta($id, 'hey_support_expertise', true);
        $meta['education'] = get_post_meta($id, 'hey_support_education', true);
        $meta['project'] = get_post_meta($id, 'hey_support_project', true);

        $meta['quote'] = get_post_meta($id, 'hey_support_quote', true);
        $meta['attribution'] = get_post_meta($id, 'hey_support_attribution', true);

        return preg_replace('/\n/', '<br/>', $meta);
    }



    public function get_case_study_meta($id, $meta = array()) {
        $meta['location'] = get_post_meta($id, 'hey_case_study_location', true);
        $meta['owner'] = get_post_meta($id, 'hey_case_study_owner', true);
        $meta['project_challenge'] = get_post_meta($id, 'hey_case_study_project_challenge', true);
        $meta['design_solution'] = get_post_meta($id, 'hey_case_study_design_solution', true);
        $meta['date'] = get_post_meta($id, 'hey_case_study_date', true);
        $meta['details'] = get_post_meta($id, 'hey_case_study_details', true);

        return preg_replace('/\n/', '<br/>', $meta);
    }


    public function get_landing_img($id, $meta, $index, $size = 'landing_feature_image') {
        $page = get_page_by_title($meta . '-' . $id, 'ARRAY_A', 'attachment');
        $img = wp_get_attachment_image_src($page['ID'], $size); 

        return $img[$index];
    }

    public function get_page_header_img($id) {
        return get_the_post_thumbnail($id, 'page_banner');
    }

    public function sidebar_project_types() {
        $senior_living = get_term_by('name', 'Senior Living', 'category');

        $senior_living_id = $senior_living->term_id;
        $senior_living = get_categories(array('child_of' => $senior_living_id, 'hide_empty' => 0));
        $nav = array();

        foreach($senior_living as $cat):
            if($cat->name != 'Uncategorized' && $cat->category_parent == $senior_living_id):
                $nav[] = array('name' => $cat->name, 'slug' => $cat->slug, 'link' => get_category_link($cat->term_id));
            endif;
        endforeach;

        include 'sidebar_project_types.php';
    }


    public function project_nav($category, $nav_thumb_src = '/wp-content/themes/ag/img/senior-living-sidebar-thumb.jpg') {
        $cats = get_categories(array('child_of' => $category, 'hide_empty' => 0));

        $nav = array();

        if(!$cats):
            $subnav = false;

            $cat_projects = get_posts(array('category' => $category, 'post_type' => 'hey_project'));

            foreach($cat_projects as $cat_project):
                $nav[] = array('name' => $cat_project->post_title, 'slug' => $cat_project->post_name);
            endforeach;
        else:
            $subnav = true;

            foreach($cats as $cat) :
                $cat_projects = get_posts(array('category' => $cat->cat_ID, 'post_type' => 'hey_project'));

                $nav[$cat->name] = array();

                foreach($cat_projects as $cat_project):
                    $nav[$cat->name][] = array('name' => $cat_project->post_title, 'slug' => $cat_project->post_name);
                endforeach;
            endforeach;
        endif;

        include 'our_work_menu.php';
    }

    public function project_gallery($id, $thumbs = false) {
            $attachments = get_posts(array(
                'post_type' => 'attachment', 
                'post_status' => null, 
                'post_parent' => $id, 
                'order' => 'ASC',
                'orderby' => 'menu_order',
                'numberposts' => -1));

            if($thumbs == false):
                $thumbs = array(476, 354);
            endif;

            $images = array();
            foreach($attachments as $attachment):
                $images[$attachment->ID] = wp_get_attachment_image_src($attachment->ID, $thumbs); 
            endforeach;

            return $images;
    }

    public function list_projects() {
        $projects = get_posts(array(
                'post_type' => 'hey_project',
                'post_status' => 'publish',
                'numberposts' => -1));

        return $projects;
    }
}

global $ag;
$ag = new HeyAG();
