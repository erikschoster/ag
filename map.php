<?php
/**
 * Template Name: National Map 
 */
?>

<?php get_header(); ?>

<script>
jQuery(document).ready(function(){
    jQuery('#map-dots').fadeIn(1400);

    jQuery('#map-sidebar a').click(function(e) {
        e.preventDefault();

        var flag_id = '#' + jQuery(this).attr('id') + '-flag';
        jQuery('#map-flags a').hide();

        jQuery(flag_id).fadeIn(400);
    });
});
</script>

<div id="breadcrumbs"></div>

<div id="custom-page-header-wrapper">
    <img src="/wp-content/themes/ag/img/map-header.jpg" />
    <h2 id="custom-page-header">National Interactive Map</h2>
</div>

<div id="map-content-wrapper">
    <div id="map-sidebar">
        <h3>Featured Projects</h3>

        <ul>
            <li><a id="map-santa-marta" href="#"><span>&#9679;</span> New CCRC's &gt;</a></li>
            <li><a id="map-eastcastle-place" href="#"><span>&#9679;</span> Repositioned CCRC's &gt;</a></li>
            <li><a id="map-st-catherines-commons" href="#"><span>&#9679;</span> Rental Communities &gt;</a></li>
            <li><a id="map-newcastle-place" href="#"><span>&#9679;</span> Assisted Living &gt;</a></li>
            <li><a id="map-cypress-glen" href="#"><span>&#9679;</span> Memory Support &gt;</a></li>
            <li><a id="map-three-crowns-park" href="#"><span>&#9679;</span> Long Term Care &gt;</a></li>
            <li><a id="map-miller-rehab-center" href="#"><span>&#9679;</span> Rehab Facilities &gt;</a></li>
            <li><a id="map-haven-hospice" href="#"><span>&#9679;</span> Hospice Care &gt;</a></li>
        </ul>
    </div>

    <div id="map-content">
        <div id="map-map">
            <div id="map-dots"></div>

            <div id="map-flags">
                <a id="map-santa-marta-flag" href="/projects/santa-marta/"><img src="/wp-content/themes/ag/img/flag-santa-marta.png" /></a>
                <a id="map-eastcastle-place-flag" href="/projects/eastcastle-place/"><img src="/wp-content/themes/ag/img/flag-eastcastle-place.png" /></a>
                <a id="map-st-catherines-commons-flag" href="/projects/st-catherine-commons/"><img src="/wp-content/themes/ag/img/flag-st-catherines-commons.png" /></a>
                <a id="map-newcastle-place-flag" href="/projects/newcastle-place/"><img src="/wp-content/themes/ag/img/flag-newcastle-place.png" /></a>
                <a id="map-cypress-glen-flag" href="/projects/cypress-glen/"><img src="/wp-content/themes/ag/img/flag-cypress-glen.png" /></a>
                <a id="map-three-crowns-park-flag" href="/projects/three-crowns-park/"><img src="/wp-content/themes/ag/img/flag-three-crowns-park.png" /></a>
                <a id="map-miller-rehab-center-flag" href="/projects/miller-rehabilitation-at-sojourn/"><img src="/wp-content/themes/ag/img/flag-miller-rehab-center.png" /></a>
                <a id="map-haven-hospice-flag" href="/projects/haven-hospice-custead-care-center/"><img src="/wp-content/themes/ag/img/flag-haven-hospice.png" /></a>
            </div>
        </div>
        
        <div id="map-clients">
            <h3>Clients</h3>

            <ul>
                <li><a href="http://www.greystonecommunities.com">Greystone Communities, INC. <small>www.greystonecommunities.com</small></a></li>
                <li><a href="http://www.harmonyrealty.com">Harmony Homes <small>www.harmonyrealty.com</small></a></li>
                <li><a href="http://www.lcsnet.com">Life Care Services <small>www.lcsnet.com</small></a></li>
                <li>Milwaukee Protestant Home</li>
                <li><a href="http://www.nlmd.com">New Life Management and Development, INC. <small>www.nlmd.com</small></a></li>
                <li><a href="http://www.saintjohnsmilw.org">Saint John's Home and Tower <small>www.saintjohnsmilw.org</small></a></li>
                <li>Three Crowns Park</li>
                <li>Smith Senior Living</li>
            </ul>
        </div>
    </div>


</div>


<div id="upper-footer">
    <?php get_template_part('footer_collab'); ?>
    <?php get_template_part('footer_engineering'); ?>
    <?php get_template_part('footer_news'); ?>
</div>

<?php get_footer(); ?>
