<div id="project-nav-sidebar">
    <img src="<?php echo $nav_thumb_src; ?>" id="our-work-nav-thumbnail" />

    <ul class="nav">
        <?php if($subnav): ?>
        <?php foreach($nav as $catname => $projects): ?>
        <li>
            <a href="#"><?php echo $catname; ?> &gt;</a>
            <ul class="subnav">
                <?php foreach($projects as $project): ?>
                <li><a href="/projects/<?php echo $project['slug']; ?>">
                    <?php echo $project['name']; ?>
                </a></li>
                <?php endforeach; ?>
            </ul>
        </li>
        <?php endforeach; ?>
        <?php else: ?>
            <?php foreach($nav as $project): ?>
                <li><a href="/projects/<?php echo $project['slug']; ?>">
                    <?php echo $project['name']; ?>
                </a></li>
            <?php endforeach; ?>
        <?php endif; ?>
    </ul>
</div>


