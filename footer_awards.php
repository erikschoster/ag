<div id="home-footer-news">
    <h3>Press Releases &amp; Awards</h3>
    <div class="home-news-teaser">
        <?php $news_query = new WP_Query(array('post_type' => array('post'), 'tag_slug__in' => array('press-releases', 'awards'), 'posts_per_page' => 2)); ?>
        <?php while($news_query->have_posts()): $news_query->the_post(); ?>
        <div class="news-teaser-wrapper">
            <h4><?php the_title(); ?></h4>
            <p>
                <a href="<?php the_permalink(); ?>">read more&gt;</a>
            </p>
        </div>
        <?php endwhile; ?>
    </div>
</div>
