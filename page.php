<?php get_header(); ?>

<?php while(have_posts()): the_post(); ?>

<?php if(has_post_thumbnail()): ?>
<div id="custom-page-header-wrapper">
    <?php echo $ag->get_page_header_img(get_the_ID()); ?>
    <h2 id="custom-page-header">
        <?php the_title(); ?>
    </h2>
</div>
<?php else: ?>
<h2 id="page-header"><?php the_title(); ?></h2>
<?php endif; ?>

<div id="page-content-wrapper">
    <?php the_content(); ?>
</div>


<div id="upper-footer">
    <?php get_template_part('footer_collab'); ?>
    <?php get_template_part('footer_engineering'); ?>
    <?php get_template_part('footer_news'); ?>
</div>

<?php endwhile; ?>

<?php get_footer(); ?>
