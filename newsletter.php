<?php
/**
 * Template Name: Newsletter 
 */
?>

<?php get_header(); ?>

<?php while(have_posts()): the_post(); ?>

<div id="custom-page-header-wrapper">
    <img src="/wp-content/themes/ag/img/contact-header.jpg" width="1020px" height="88px" />
    <h2 id="custom-page-header">
        <?php the_title(); ?>
    </h2>
</div>

<div id="page-content-wrapper" class="contact-form">
    <div id="message"></div>
    <form id="email-form" name="email-form" action="/wp-content/themes/ag/e.php" method="post">
        <div id="contact-left">
            <fieldset>
                <label for="contact-name">
                    Name
                </label>
                <input type="text" name="contact-name" value="" />
            </fieldset>

            <fieldset>
                <label for="e">
                    Email
                </label>
                <input type="text" name="e" value="" />
            </fieldset>

            <fieldset>
                <label for="contact-phone">
                    Phone Number
                </label>
                <input type="text" name="contact-phone" value="" />
            </fieldset>

            <fieldset>
                <label for="contact-type">
                    Type of Project
                </label>
                <input type="text" name="contact-type" value="" />
            </fieldset>

            <fieldset>
                <label for="contact-company">
                    Company Name
                </label>
                <input type="text" name="contact-company" value="" />
            </fieldset>

            <fieldset>
                <label for="contact-message">
                    Message
                </label>
                <textarea name="contact-message"></textarea>
            </fieldset>


            <input type="hidden" class="checkbox" name="contact-newsletter" value="1" />
            <input type="hidden" name="email" value="Email Address" />
            <input type="hidden" name="name" value="" />

            <button type="submit" id="contact-submit" name="submit">Submit</button>

        </div>


    </form>
</div> 

<div id="upper-footer">
    <?php get_template_part('footer_collab'); ?>
    <?php get_template_part('footer_engineering'); ?>
    <?php get_template_part('footer_news'); ?>
</div>



<?php endwhile; ?>

<script>
jQuery(document).ready(function(){
    jQuery("#email-form").submit(function(e) {
        e.preventDefault();
        eform = jQuery(this);

        jQuery.post(eform.attr('action'), eform.serialize() + '&ajax=1', function(data) {
            jQuery("div#form-wrapper").fadeOut('fast');
            jQuery("div#message").html(data).fadeIn('fast');

            if(jQuery("div#message p").hasClass("error") == true) {
                jQuery("div#message").delay(1000).fadeOut('fast', function() {
                    jQuery("div#form-wrapper").fadeIn('fast');
                });
            } else {
                jQuery('#contact-submit').remove();
            }
        });
    });

});
</script>


<?php get_footer(); ?>
