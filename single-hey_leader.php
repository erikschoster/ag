<?php get_header(); ?>
<?php while(have_posts()): the_post(); ?>

<div id="breadcrumbs"></div>

<div id="blog-content-wrapper">

    <h2 style="background-image: url(/wp-content/themes/ag/img/leader-header.jpg);" id="leader-header">Leadership Team</h2>

    <div id="leader-sidebar">
        <?php $leaders = get_posts(array('post_type'=>'hey_leader', 'numberposts'=>-1)); ?>
        <ul>
        <?php foreach($leaders as $leader): ?>
            <?php $current_leader = ($leader->ID == get_the_ID()) ? 'orange' : 'white'; ?>
            <li><a class="<?php echo $current_leader; ?>" href="<?php echo get_permalink($leader->ID); ?>"><?php echo $leader->post_title; ?></a></li>
        <?php endforeach; ?>
            <li id="meet-the-associates"><a href="/associates/bill-boehler/">Meet the<br/>Senior Associates &gt;</a></li>
            <li><a href="/supports/amber-bahr/">Meet the AG Team &gt;</a></li>
      </ul>
    </div>

    <div id="blog-content" class="leader-content">
        <?php $meta = $ag->get_leader_meta(get_the_ID()); ?>

        <div id="leader-meta-left">
            <?php $images = $ag->project_gallery(get_the_ID(), 'leader_thumb'); ?>
            <?php foreach($images as $image): ?>
                <img src="<?php echo $image[0]; ?>" />
            <?php endforeach; ?>

          
 
            <h4>Areas of Expertise:</h4>
            <p><?php echo $meta['expertise']; ?></p>
 
            <h4>Education:</h4>
            <p><?php echo $meta['education']; ?></p>
 
        </div>
 
        <div id="leader-meta-right">
            <h3><?php the_title(); ?></h3>

            <p><?php echo $meta['job_title']; ?></p>
            <p><?php echo $meta['bio']; ?></p>

            <?php if(!empty($meta['project'])): ?>
            <div id="signature-project-wrapper">
            <div id="signature-project">
                <?php $images = $ag->project_gallery($meta['project'], 'medium'); ?>
                <?php $image = array_shift($images); ?>
                <img src="<?php echo $image[0]; ?>" />
            </div>
            <div id="signature-project-info">
                <p><strong>Signature Project:</strong></p>             
                <p><?php echo get_the_title($meta['project']); ?><br/>
                <?php echo get_post_meta($meta['project'], 'hey_project_location', true); ?></p>
            </div>
            </div>
            <?php endif; ?>

            <blockquote><?php echo get_post_meta(get_the_ID(), 'hey_leader_quote', true); ?>
            <p><em><?php echo get_post_meta(get_the_ID(), 'hey_leader_attribution', true); ?></em></p>
            </blockquote>
        </div>
    </div>

</div>

<div id="upper-footer">
    <?php get_template_part('footer_collab'); ?>
    <?php get_template_part('footer_experience'); ?>
    <?php get_template_part('footer_awards'); ?>
</div>

<?php endwhile; ?>
<?php get_footer(); ?>
