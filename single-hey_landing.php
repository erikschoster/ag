<?php get_header(); ?>
<?php while(have_posts()): the_post(); ?>

<div id="breadcrumbs"></div>

<div id="project-landing-wrapper">
    <h2 style="background-image: url(<?php echo $ag->get_landing_img(get_the_ID(), 'hey_landing_banner', 0, 'landing_banner'); ?>);" id="landing-header"><?php the_title(); ?></h2>

    <div id="project-content-wrapper">
        <?php $category = get_category_by_slug($post->post_name); ?>
        <?php $thumb_src = $ag->get_landing_img(get_the_ID(), 'hey_landing_thumb', 0, 'landing_thumb'); ?>
        <?php $ag->project_nav($category->cat_ID, $thumb_src); ?>

        <div id="project-landing-feature">
            <img src="<?php echo $ag->get_landing_img(get_the_ID(), 'hey_landing_feature', 0, 'landing_feature_image'); ?>" /> 
        </div>

        <div class="project-content" id="<?php echo $post->post_name; ?>-content">
            <h3><?php echo get_post_meta(get_the_ID(), 'hey_landing_subheading', true); ?></h3>
            <p><?php echo get_post_meta(get_the_ID(), 'hey_landing_description', true); ?></p>
        </div>
    </div>
</div>

<div id="upper-footer">
    <?php get_template_part('footer_collab'); ?>
    <?php get_template_part('footer_experience'); ?>
</div>

<?php endwhile; ?>
<?php get_footer(); ?>
