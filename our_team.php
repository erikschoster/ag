<?php
/**
 * Template Name: Our Team 
 */
?>
<?php get_header(); ?>
<?php while(have_posts()): the_post(); ?>

<div id="breadcrumbs"></div>

<div id="blog-content-wrapper">

    <h2 id="ourteam-header">Our Team</h2>

    <div id="ourteam-sidebar">
        <?php $leaders = get_posts(array('post_type'=>'hey_leader', 'numberposts'=>-1)); ?>
        <ul>
        <?php foreach($leaders as $leader): ?>
            <?php $current_leader = ($leader->ID == get_the_ID()) ? 'orange' : 'white'; ?>
            <li><a class="<?php echo $current_leader; ?>" href="<?php echo get_permalink($leader->ID); ?>"><?php echo $leader->post_title; ?></a></li>
        <?php endforeach; ?>
            <li id="meet-the-associates"><a href="/associates/bill-boehler/">Meet the <br/>Senior Associates &gt;</a></li>
  <li><a href="/supports/amber-bahr/">Meet the AG Team &gt;</a></li>
        </ul>
    </div>

    <div id="blog-content" class="ourteam-content">
        <div id="ourteam-meta-left">
            <img src="/wp-content/uploads/2012/06/team_montage2.jpg" /> 
        </div>
 
        <div id="ourteam-meta-right">
            <h3>Who We Are</h3>
            <?php the_content(); ?>
        </div>
    </div>

</div>

<div id="upper-footer">
    <?php get_template_part('footer_collab'); ?>
    <?php get_template_part('footer_experience'); ?>
    <?php get_template_part('footer_news'); ?>
</div>

<?php endwhile; ?>
<?php get_footer(); ?>
