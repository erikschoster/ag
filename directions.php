<?php
/**
 * Template Name: Directions
 */
?>

<?php get_header(); ?>

<?php while(have_posts()): the_post(); ?>

<div id="custom-page-header-wrapper">
    <img src="/wp-content/themes/ag/img/directions-header.jpg" width="1020px" height="88px" />
    <h2 id="custom-page-header">
        <?php the_title(); ?>
    </h2>
</div>

<div id="page-content-wrapper" class="directions">
    <img src="/wp-content/themes/ag/img/directions-map.png" id="directions-map" />
    <img src="/wp-content/themes/ag/img/directions-photo.jpg" id="directions-photo" />

    <div id="directions-content">
        <?php the_content(); ?>
    </div>

    <div id="directions-address">
        <address>
            1414 Underwood Avenue Suite 301<br/>
            Wauwatosa, WI 53213<br/>
            phone: (414) 431-3131<br/>
            fax: (414) 431-0531 
        </address>
    </div> 
</div> 

<div id="upper-footer">
    <?php get_template_part('footer_collab'); ?>
    <?php get_template_part('footer_engineering'); ?>
    <?php get_template_part('footer_news'); ?>
</div>



<?php endwhile; ?>

<?php get_footer(); ?>
