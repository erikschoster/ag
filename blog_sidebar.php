<?php global $ag; ?>
    <div id="blog-sidebar">
        <div id="sidebar-project-types">
            <h4>Project Types</h4>

            <ul>
                <?php $ag->sidebar_project_types(); ?>
                <li><a href="/category/affordable-housing/">Affordable Housing &gt;</a></li>
                <li><a href="/category/multi-family/">Multi-Family &gt;</a></li>
                <li><a href="/category/mixed-use/">Mixed Use / Retail &gt;</a></li>
                <li><a href="/category/student-housing/">Student Housing &gt;</a></li>
            </ul>
        </div>

        <div id="sidebar-whats-going-on">
            <h4>What's Going On</h4>

            <ul>
                <li><a href="/tag/on-the-board/">On the Board &gt;</a></li>
                <li><a href="/tag/in-the-field/">In the Field &gt;</a></li>
                <li><a href="/tag/contributors/">Contributors &gt;</a></li>
                <li><a href="/tag/employment/">Employment &gt;</a></li>
                <li><a href="/tag/team/">Team &gt;</a></li>
                <li><a href="/tag/press-releases-awards/">Press Releases &amp; Awards &gt;</a></li>
            </ul>
        </div>
    </div>

