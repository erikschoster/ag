<?php
    require('hey.php');
    require('ag.php');

    $hey->thumb_size('project_thumb', 'Project Thumbnail', 73, 66);
    $hey->thumb_size('project_image', 'Project Image', 476, 354);
    $hey->thumb_size('case_study_image', 'Case Study Image', 368, 215);
    $hey->thumb_size('landing_feature_image', 'Landing Feature Image', 477, 425);
    $hey->thumb_size('landing_thumb', 'Landing Thumb', 161, 104);
    $hey->thumb_size('landing_banner', 'Landing Banner', 1020, 90);
    $hey->thumb_size('page_banner', 'Page Banner', 1020, 88);
    $hey->thumb_size('leader_thumb', 'Leader Thumb', 235, 150);
    $hey->thumb_size('support_thumb', 'Leader Thumb', 300, 175);

    // Global js
    $hey->script('less', 'less.min.js');
    $hey->script('jquery');
    $hey->script('jquery-cycle', 'jquery.cycle.all.js', 'jquery');
    $hey->script('home', 'home.js', 'jquery-cycle', array('home', true));
    $hey->script('header', 'header.js', 'jquery');
    
    // Blog Case Study
    $hey->script('project', 'project.js', 'jquery', array('post_type', 'hey_case_study'));
    $hey->post_supports = array('title');
    $hey->post_taxonomies = array('post_tag', 'category');
    $hey->post_type('case_study', 'Case Study', 'Case Studies', 'case-studies');
    $hey->post_meta('location', 'Location', 'hey_case_study');
    $hey->post_meta('owner', 'Owner', 'hey_case_study');
    $hey->post_meta('project_challenge', 'Project Challenge', 'hey_case_study');
    $hey->post_meta('design_solution', 'Design Solution', 'hey_case_study');
    $hey->post_meta('date', 'Date of Completion', 'hey_case_study');
    $hey->post_meta('details', 'Details', 'hey_case_study');
    $hey->post_meta('images', 'Images', 'hey_case_study');

    // Leadership Team Member 
    $hey->script('leader', 'leader.js', 'jquery', array('post_type', 'hey_leader'));
    $hey->post_supports = array('title', 'thumbnail');
    $hey->post_taxonomies = array();
    $hey->post_type('leader', 'Leadership Team Member');
    $hey->post_meta('job_title', 'Job Title', 'hey_leader');
    $hey->post_meta('bio', 'Biography', 'hey_leader');
    //$hey->post_meta('experience', 'Years of Experience', 'hey_leader');
    //$hey->post_meta('project_position', 'Project Position', 'hey_leader');
    $hey->post_meta('project', 'Significant Project', 'hey_leader');
    $hey->post_meta('expertise', 'Areas of Expertise', 'hey_leader');
    $hey->post_meta('education', 'Education', 'hey_leader');
    $hey->post_meta('details', 'Details', 'hey_leader');
    $hey->post_meta('quote', 'Quote', 'hey_leader');
    $hey->post_meta('attribution', 'Quote Attribution', 'hey_leader');

    // Associate Team Member 
    //$hey->script('leader', 'leader.js', 'jquery', array('post_type', 'hey_associate'));
    $hey->post_associates = array('title', 'thumbnail');
    $hey->post_taxonomies = array();
    $hey->post_type('associate', 'Associate Team Member');
    $hey->post_meta('job_title', 'Job Title', 'hey_associate');
    $hey->post_meta('bio', 'Biography', 'hey_associate');
    //$hey->post_meta('experience', 'Years of Experience', 'hey_associate');
    //$hey->post_meta('project_position', 'Project Position', 'hey_associate');
    $hey->post_meta('project', 'Significant Project', 'hey_associate');
    $hey->post_meta('expertise', 'Areas of Expertise', 'hey_associate');
    $hey->post_meta('education', 'Education', 'hey_associate');
    $hey->post_meta('details', 'Details', 'hey_associate');
    $hey->post_meta('quote', 'Quote', 'hey_associate');
    $hey->post_meta('attribution', 'Quote Attribution', 'hey_associate');

    // Support Team Member 
    //$hey->script('leader', 'leader.js', 'jquery', array('post_type', 'hey_support'));
    $hey->post_supports = array('title', 'thumbnail');
    $hey->post_taxonomies = array();
    $hey->post_type('support', 'Support Team Member');
    $hey->post_meta('bio', 'Biography', 'hey_support');
    //$hey->post_meta('experience', 'Years of Experience', 'hey_support');
    //$hey->post_meta('project_position', 'Project Position', 'hey_support');
    $hey->post_meta('project', 'Significant Project', 'hey_support');
    $hey->post_meta('expertise', 'Areas of Expertise', 'hey_support');
    $hey->post_meta('education', 'Education', 'hey_support');
    $hey->post_meta('details', 'Details', 'hey_support');
    $hey->post_meta('quote', 'Quote', 'hey_support');
    $hey->post_meta('attribution', 'Quote Attribution', 'hey_support');

    // Category Landing 
    $hey->post_supports = array('title', 'featured_image');
    $hey->post_taxonomies = array();
    $hey->post_type('landing', 'Landing Page', null, 'our-work');
    $hey->post_meta('feature', 'Feature Image', 'hey_landing');
    $hey->post_meta('banner', 'Banner Image', 'hey_landing');
    $hey->post_meta('thumb', 'Thumbnail Image', 'hey_landing');
    $hey->post_meta('subheading', 'Subheading', 'hey_landing');
    $hey->post_meta('description', 'Description', 'hey_landing');

    // Projects
    $hey->script('project', 'project.js', 'jquery', array('post_type', 'hey_project'));
    $hey->post_supports = array('title');
    $hey->post_taxonomies = array('category');
    $hey->post_type('project', 'Project');
    $hey->post_meta('location', 'Project Location (for team member signature projects)', 'hey_project');
    $hey->post_meta('details', 'Project Details', 'hey_project');
    $hey->post_meta('description', 'Project Description', 'hey_project');
    $hey->post_meta('case_study', 'Case Study Link', 'hey_project');
    $hey->post_meta('quote', 'Quote', 'hey_project');
    $hey->post_meta('quote-attribution', 'Quote Attribution', 'hey_project');
    $hey->post_meta('images', 'Project Images', 'hey_project');

    // Fire actions
    $hey->add_actions();
