<div id="home-slideshow-nav"></div>

<div id="home-slideshow">
    <div class="home-slide">
        <div class="home-slide-header">
            <h3>New CCRC</h3>
            <h4>Edgewater</h4>
            <p>West Des Moines, Iowa<br/>
            <a href="/projects/edgewater/">more info&gt;</a></p>
        </div>
     <div class="home-slide-caption">
            <p><a href="/our-work/senior-living/">Senior Living &gt;</a></p>
        </div>
        <img src="/wp-content/themes/ag/img/ag-senior-living.jpg" />
    </div>

     <div class="home-slide">
        <div class="home-slide-header">
            <h3>New CCRC</h3>
            <h4>Newcastle Estates</h4>
            <p>Mequon, Wisconsin<br/>
            <a href="/projects/newcastle-estates/">more info&gt;</a></p>
        </div>

       <div class="home-slide-caption">
            <p><a href="/our-work/senior-living/">Senior Living &gt;</a></p>
        </div>
        <img src="/wp-content/uploads/2012/05/newcastle_02.jpg" />
    </div>

    <div class="home-slide">
        <div class="home-slide-header">
            <h3>Repositioned CCRC</h3>
            <h4>Eastcastle Place</h4>
            <p>Wauwatosa, Wisconsin <br/>
            <a href="/projects/eastcastle-place/">more info&gt;</a></p>
        </div>
         <div class="home-slide-caption">
            <p><a href="/our-work/senior-living/">Senior Living &gt;</a></p>
        </div>
        <img src="/wp-content/uploads/2012/05/eastcastle_03.jpg" />
    </div>

    <div class="home-slide">
        <div class="home-slide-header">
            <h3>New CCRC</h3>
            <h4>Aberdeen</h4>
            <p>Waukesha, Wisconsin<br/>
            <a href="/projects/aberdeen/">more info&gt;</a></p>
        </div>
           <div class="home-slide-caption">
            <p><a href="/our-work/senior-living/">Senior Living &gt;</a></p>
        </div>
        <img src="/wp-content/uploads/2012/05/aberdeen_04.jpg" />
    </div>

    <div class="home-slide">
        <div class="home-slide-header">
            <h3>New CCRC</h3>
            <h4>Santa Marta</h4>
            <p>Olathe, Kansas <br/> 
		    <a href="/projects/santa-marta/">more info&gt;</a></p>
        </div>
     <div class="home-slide-caption">
            <p><a href="/our-work/senior-living/">Senior Living &gt;</a></p>
        </div>
        <img src="/wp-content/uploads/2012/05/santamarta_05.jpg" />
    </div>
 
</div>


