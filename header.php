<!DOCTYPE html>
<html>
<head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <title><?php wp_title(); ?> <?php bloginfo('name'); ?></title>

    <link rel="stylesheet" href="/wp-content/themes/ag/css/reset.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
    <link rel="stylesheet/less" href="/wp-content/themes/ag/css/main.less?<?php echo rand(); ?>" type="text/css" media="screen" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <link rel="shortcut icon" href="/wp-content/themes/ag/img/ag_favicon.png" />

    <?php wp_head(); ?>
</head>
<body>
<div id="main-wrapper">

<div id="header">
    <div id="logo">
        <a href="/">AG Architecture</a>
    </div>

    <div id="search-nav">
        <p>Contact us by <a href="#">email</a> or call: 414.431.3131 <span>&bull;</span></p>
        <p><a class="newsletter" href="/newsletter/">Sign up for our newsletter</a></p>
        <a href="https://www.facebook.com/AGneighborhood" target="blank"><img class="facebook" src="/wp-content/themes/ag/img/facebook.png" alt="Facebook" /></a> 
        <a href="https://twitter.com/#!/AGneighborhood" target="blank"><img class="twitter" src="/wp-content/themes/ag/img/twitter.png" alt="Twitter" /></a> 
        <input type="search" value="" name="s" id="s" placeholder="SEARCH" />
    </div>

    <div id="navigation">
        <ul>
            <li><a href="/">Home</a></li>
            <li class="dot">&bull;</li>
            <li class="subtrigger">
                <a href="/our-firm/about-us/">Our Firm</a>

                <div class="subnavigation" id="our-firm-subnavigation">
                    <ul>
                        <li><a href="/our-firm/about-us/">About Us</a></li>
                        <li class="dot">&bull;</li>
                        <li><a href="/our-firm/history/">History</a></li>
                        <li class="dot">&bull;</li>
                        <li><a href="/our-firm/services/">Services</a></li>
                        <li class="dot">&bull;</li>
                        <li><a href="/our-firm/our-team/">Our Team</a></li>
                    </ul>
                </div>

            </li>

            <li class="dot">&bull;</li>
            <li class="subtrigger">
                <a href="/our-work/senior-living/">Our Work</a>

                <div class="subnavigation" id="our-work-subnavigation">
                    <ul>
                        <li><a href="/our-work/senior-living/">Senior Living</a></li>
                        <li class="dot">&bull;</li>
                        <li><a href="/our-work/affordable-housing/">Affordable Housing</a></li>
                        <li class="dot">&bull;</li>
                        <li><a href="/our-work/multi-family/">Multi-Family</a></li>
                        <li class="dot">&bull;</li>
                        <li><a href="/our-work/mixed-use/">Mixed Use / Retail</a></li>
                        <li class="dot">&bull;</li>
                        <li><a href="/our-work/student-housing/">Student Housing</a></li>
                    </ul>
                </div>
            </li>


            <li class="dot">&bull;</li>
            <li>
                <a href="/our-process/">Our Process</a>
            </li>

            <li class="dot">&bull;</li>
            <li><a href="/engineering/">Engineering</a></li>


            <li class="dot">&bull;</li>
            <li class="subtrigger">
                <a href="/contact-us/contact-form/">Contact Us</a>

                <div class="subnavigation" id="contact-us-subnavigation">
                    <ul>
                        <li><a href="/contact-us/contact-form/">Contact Form</a></li>
                        <li class="dot">&bull;</li>
                        <li><a href="/contact-us/directions/">Directions</a></li>
                    </ul>
                </div>

            </li>

            <li class="dot">&bull;</li>
            <li><a href="/news/">News / Blog</a></li>
        </ul>
    </div>

    <?php if(!is_home()): ?>
    <ul id="breadcrumbs">
        <li><a href="/">Home &gt;</a></li> 

        <?php if(is_page()): ?>

            <?php $parent = get_post($post->post_parent); ?>
            <?php if ($parent->post_title != $post->post_title): ?>
            <li><?php echo $parent->post_title; ?> &gt;</li>
            <?php endif; ?>
            <li><a href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a></li>

        <?php elseif(is_single()): ?>

<?php switch($post->post_type): case 'post': ?>
        <li><a href="/news/">News &gt;</a></li>
        <?php break; ?>
<?php case 'hey_leader': ?>
<?php case 'hey_associate': ?>
<?php case 'hey_support': ?>
        <li><a href="/our-firm/our-team/">Our Team &gt;</a></li>
        <?php break; ?>
<?php case 'hey_landing': ?>
<?php case 'hey_project': ?>
        <li><a href="/our-work/senior-living/">Our Work &gt;</a></li>
        <?php break; ?>
<?php endswitch; ?>

            <?php $bcats = get_the_category(); ?>
            <?php foreach($bcats as $bcat): ?>
                <li><?php echo $bcat->cat_name; ?> &gt;</li>
            <?php endforeach; ?>
            <li><a href="<?php echo the_permalink(); ?>"><?php echo the_title(); ?></a></li>

        <?php endif; ?>
    </ul>
    <?php endif; ?>
</div>


