<div id="upper-footer-collab">
    <h3>Our Collaborative Process</h3>

    <p>You have a vision, a goal, an idea. It’s our task to listen, interpret, articulate a solution and build consensus. Together, working hand in hand with your development team, we will implement it. The AG collaborative process has been an integral component of our ability to build long-term relationships with our clients. <a href="/our-process/">more info&gt;</a></p>
</div>
