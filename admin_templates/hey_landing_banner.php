<?php $id = get_post_meta($post->ID, $meta['id'], true); ?>

<div style="display: block; overflow: hidden; zoom: 1;">

<?php if(is_numeric($id)): ?>
    <div style="float: right; padding: 10px; border: 1px solid #ddd; margin: 10px;">
        <?php $img = wp_get_attachment_image_src($id, array(220, 220)); ?>
        <img src="<?php echo $img[0]; ?>" />
    </div>
<?php endif; ?>

    <p>Upload a new banner:</p>
    <input type='file' name='<?php echo $meta['id']; ?>' />

</div>
