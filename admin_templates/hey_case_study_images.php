<script>
jQuery(document).ready(function(){
    jQuery('#hey-admin-thumbs').sortable({
        update: function(e,u) { 
            var imageorder = jQuery(this).sortable("toArray"); 
            jQuery('input#hey-image-order-vals').val(imageorder);
        } 
    });
});
</script>

<?php
   $images = HeyAG::project_gallery($post->ID, 'project_thumb');     
?>

<p>Drag to reorder images. To upload and remove images, please use the media area.</p>

<div id="hey-admin-thumbs">
<?php foreach($images as $image_id => $image): ?>
    <img style="padding: 3px;" src="<?php echo $image[0]; ?>" id="<?php echo $image_id; ?>" />
<?php endforeach; ?>
</div>

<input type="hidden" name="<?php echo $meta['id']; ?>" value="<?php echo $meta['value']; ?>" id="hey-image-order-vals"/>
