<?php $projects = HeyAG::list_projects(); ?>
<select name="meta_boxes[<?php echo $meta['id']; ?>]">
    <option value=""></option>
<?php foreach($projects as $index => $project): ?>
    <?php $selected = ($meta['value'] == $project->ID) ? 'selected="selected"' : ''; ?>
    <option value="<?php echo $project->ID; ?>" <?php echo $selected; ?>><?php echo $project->post_title; ?></option>
<?php endforeach; ?>
</select>

