<?php

class HeyWP {
    static $prefix = 'hey_';
    static $nonce_url = 'foobar';

    public $admin_template_directory = 'admin_templates';

    public $post_public = true;
    public $post_position = 5;
    public $post_supports = array('title', 'editor');
    public $post_taxonomies = array();
    public $post_plural_name = null;
    public $post_display_name = null;

    private $post_types = array();
    private $post_meta_items = array();
    private $post_taxonomy_terms = array();
    private $scripts = array();

    function __construct() {
        add_action('post_edit_form_tag', array(&$this, 'img_form_tag'));
    }

    public function img_form_tag() {
        echo ' enctype="multipart/form-data"';
    }

    public function add_actions() {
        add_action('init', array(&$this, 'add_post_types'));

        foreach($this->post_types as $post):
            add_action('publish_' . self::$prefix . $post['post_name'], array(&$this, 'add_post_meta'));
            add_action('add_meta_boxes_' . self::$prefix . $post['post_name'], array(&$this, 'add_post_meta'));
        endforeach;

        add_action('save_post', array(&$this, 'save_post_meta'));
        add_action('wp_enqueue_scripts', array(&$this, 'add_scripts'));

        if(!empty($this->thumb_sizes)):
            add_theme_support('post-thumbnails');

            foreach($this->thumb_sizes as $size):
                add_image_size($size['name'], $size['width'], $size['height'], true);
            endforeach;

            add_filter('image_size_names_choose', array(&$this, 'add_thumb_sizes'));
        endif;
    }

    public function clear() {
        $this->post_types = array();
    }

    public function add_thumb_sizes($sizes) {
        $custom_sizes = array();
        foreach($this->thumb_sizes as $size):
            $custom_sizes[$size['name']] = $size['label'];
        endforeach;

        return array_merge($sizes, $custom_sizes);
    }

    public function thumb_size($name, $label, $width, $height) {
        $thumb_size = array();

        $thumb_size['name']   = $name;
        $thumb_size['label']  = $label;
        $thumb_size['width']  = $width;
        $thumb_size['height'] = $height;

        $this->thumb_sizes[] = $thumb_size;

        return $thumb_size;
    }

    // Add taxonomy
    public function taxonomy($id, $label, $label_plural=null) {
        $taxonomy = array();

        $taxonomy['id'] = $id;
        $taxonomy['label'] = $label;

        $taxonomy['sort'] = $this->taxonomy_sort;

        if($label_plural == null):
            $taxonomy['label_plural'] = $label . 's';
        else:
            $taxonomy['label_plural'] = $label_plural;
        endif;

        $this->post_taxonomy_terms[] = $taxonomy;

        return $taxonomy;
    }

    // Add post meta
    public function post_meta($post_id, $post_label, $post_type) {
        $post_meta = array();

        $post_meta['post_id'] = $post_type . '_' . $post_id;
        $post_meta['post_label'] = $post_label;
        $post_meta['post_type'] = $post_type;

        $this->post_meta_items[] = $post_meta;

        return $post_meta;
    }

    public function post_meta_ui($post, $meta) {
        wp_nonce_field(self::$nonce_url, self::$prefix . 'nonce');

        $meta['value'] = get_post_meta($post->ID, $meta['id'], true);

        include($this->admin_template_directory . '/' . $meta['id'] . '.php');
    }

    public function add_post_meta() {
        foreach($this->post_meta_items as $post_meta):
            add_meta_box($post_meta['post_id'], __($post_meta['post_label']), array(&$this, 'post_meta_ui'), $post_meta['post_type']);
        endforeach;
    }

    public function save_post_meta($post_id) {
        if(empty($_POST)):
            return;
        endif;

        if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE):
            return;
        endif;

        if(!current_user_can('edit_post', $post_id)):
            return;
        endif;

        if(isset($_POST[self::$prefix . 'nonce']) && check_admin_referer(self::$nonce_url, self::$prefix . 'nonce')):
            // Unhook self to prevent loops
            remove_action('save_post', array(&$this, 'save_post_meta'));

            // Insert project image attachment order if set
            if(isset($_POST['hey_project_images']) && !empty($_POST['hey_project_images'])):
                foreach(explode(',', $_POST['hey_project_images']) as $imageorder => $imageid):
                    wp_update_post(array('ID' => $imageid, 'menu_order' => $imageorder));
                endforeach;
            endif;

            // Insert case study image attachment order if set
            if(isset($_POST['hey_case_study_images']) && !empty($_POST['hey_case_study_images'])):
                foreach(explode(',', $_POST['hey_case_study_images']) as $imageorder => $imageid):
                    wp_update_post(array('ID' => $imageid, 'menu_order' => $imageorder));
                endforeach;
            endif;

            // Save meta boxes
            if(isset($_POST['meta_boxes'])):
                foreach($_POST['meta_boxes'] as $meta_id => $meta_value):
                    update_post_meta($post_id, $meta_id, $meta_value);
                endforeach;
            endif;

            // Rehook when finished
            add_action('save_post', array(&$this, 'save_post_meta'));
        endif;

        
        // Save uploaded images
        if(isset($_FILES) && !empty($_FILES)):

            foreach($_FILES as $meta_id => $file):
            if($file['size'] > 0):

                $file_type = wp_check_filetype(basename($file['name']));

                $allowed_file_types = array('image/jpg', 'image/jpeg', 'image/gif', 'image/png');

                if(in_array($file_type['type'], $allowed_file_types)):
                    $upload_args = array('test_form' => false);
                    $upload = wp_handle_upload($file, $upload_args);

                    if(isset($upload['file'])):
                        $attachment = array(
                            'post_mime_type' => $file_type['type'],
                            'post_title' => $meta_id . '-' . $post_id,
                            'post_content' => '',
                            'post_status' => 'inherit',
                        );

                        $attach_id = wp_insert_attachment($attachment, $upload['file']);
                        require_once(ABSPATH . "wp-admin/includes/image.php");
                        $attach_data = wp_generate_attachment_metadata($attach_id, $upload['file']);
                        wp_update_attachment_metadata($attach_id, $attach_data);

                        $existing_upload = get_post_meta($post_id, $meta_id, true);
                        if(is_numeric($existing_upload)):
                            wp_delete_attachment($existing_upload);
                        endif;

                        update_post_meta($post_id, $meta_id, $attach_id);

                    endif;
                endif;

            endif;
            endforeach;
        endif;
    }

    // Add post type
    public function post_type($name, $display_name=null, $plural_name=null, $rewrite=null) {
        $post_type = array();

        $post_type['post_name'] = $name;

        $post_type['post_public'] = $this->post_public;
        $post_type['post_position'] = $this->post_position;
        $post_type['post_supports'] = $this->post_supports;
        $post_type['post_taxonomies'] = $this->post_taxonomies;

        if($rewrite != null):
            $post_type['rewrite'] = $rewrite;
        endif;

        if($display_name == null):
            $post_type['post_display_name'] = ucwords($name);
        else:
            $post_type['post_display_name'] = $display_name;
            $name = $display_name;
        endif;

        if($plural_name == null):
            $post_type['post_plural_name'] = ucwords($name) . 's';
        else:
            $post_type['post_plural_name'] = $plural_name;
        endif;

        $post_type['post_singular_name'] = ucwords($name);

        $this->post_types[] = $post_type;

        return $post_type;
    }

    public function add_post_types() {
        foreach($this->post_types as $post_type):

                if(!isset($post_type['rewrite'])):
                    $post_type['rewrite'] = $post_type['post_name'] . 's';
                endif;

                $post_args = array(
                    'labels' => array(
                        'name'          => __($post_type['post_singular_name']),
                        'menu_name'     => __($post_type['post_display_name']),
                        'all_items'     => __($post_type['post_plural_name']),
                        'singular_name' => __($post_type['post_singular_name']),
                        'edit_item'     => __('Edit ' . $post_type['post_singular_name']),
                        'add_new'       => __('Add ' . $post_type['post_singular_name']),
                        'add_new_item'  => __('Add New ' . $post_type['post_singular_name']),
                        'new_item'      => __('New ' . $post_type['post_singular_name']),
                        'view_item'     => __('View ' . $post_type['post_singular_name']),
                        'search_items'  => __('Search ' . $post_type['post_plural_name']),
                        'not_found'     => __('No ' . strtolower($post_type['post_plural_name']) . ' found'),
                    ),

                    'public'            => $post_type['post_public'],
                    'has_archive'       => $post_type['post_name'] . 's',
                    'rewrite'           => array('slug' => $post_type['rewrite']),
                    'supports'          => $post_type['post_supports'],
                    'taxonomies'        => $post_type['post_taxonomies'],
                    'menu_position'     => $post_type['post_position'],
                );

                register_post_type(self::$prefix . $post_type['post_name'], $post_args);
        endforeach;
    }

    public function script($slug, $filename = null, $deps = null, $context = null) {
        $script = array();
        $script['slug'] = $slug;
        $script['filename'] = $filename;
        $script['context'] = $context;

        if($deps == null):
            $script['deps'] = array();
        else:
            $script['deps'] = array($deps);
        endif;

        $this->scripts[] = $script;

        return $script;
    }

    private function check_context($context) {
        switch($context[0]):
            case 'home':
                return is_home() == $context[1];
            case 'post_type':
                return $context[1] == get_post_type();

            break;
        endswitch;

        return false;
    }

    public function add_scripts() {
        foreach($this->scripts as $script):
            if($script['context'] == null || ($script['context'] != null && $this->check_context($script['context']))):
                if($script['filename'] == null):
                    wp_enqueue_script($script['slug']);
                else:
                    wp_enqueue_script($script['slug'], get_template_directory_uri() . '/js/' . $script['filename'], $script['deps']);
                endif;
            endif;
        endforeach;
    }

}

$hey = new HeyWP();
