<?php get_header(); ?>

<?php while(have_posts()): the_post(); ?>

<h2 id="blog-recent-posts">Recent Posts</h2>

<div id="blog-content-wrapper">
    <div id="blog-content">
        <h3 id="blog-header"><?php the_title(); ?></h3>
        <p><?php the_date(); ?>, In News, Recent Posts, by <?php the_author(); ?></p>
        <?php the_content(); ?>
    </div>
    
    <?php get_template_part('blog_sidebar'); ?>
</div>


<div id="upper-footer">
    <?php get_template_part('footer_collab'); ?>
    <?php get_template_part('footer_engineering'); ?>
    <?php get_template_part('footer_news'); ?>
</div>

<?php endwhile; ?>

<?php get_footer(); ?>
