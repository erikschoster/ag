<?php
/**
 * Template Name: News 
 */
?>
<?php error_reporting(-1); ?>
<?php get_header(); ?>

<?php $tag = get_term_by('slug', $wp_query->query_vars['tag'], 'post_tag'); ?>

<h2 id="blog-recent-posts"><?php echo $tag->name; ?></h2>
<div id="blog-content-wrapper">
    <div id="blog-content" class="news-landing">

    <?php while(have_posts()): the_post(); ?>
    <div class="post-wrapper">
        <h3 class="post-header"><?php the_title(); ?></h3>
        <p class="post-leader"><?php the_date(); ?>, <?php the_tags('In ', ', ', ','); ?> by <?php the_author(); ?></p>

        <?php if(has_post_thumbnail()): ?>
        <div class="post-thumb">
            <?php echo get_the_post_thumbnail(get_the_ID(), 'medium'); ?>
        </div>
        <?php endif; ?>

        <?php $content = get_the_content(); ?>
        <?php $content = strip_tags($content, '<p><b><i><strong><em><div><li><ul>'); ?>
        <?php $content = apply_filters('the_content', $content); ?>
        <?php echo substr($content, 0, 500); ?>...
        <a href="<?php the_permalink(); ?>" class="post-readmore">Read more...</a>
    </div>
    <?php endwhile; ?>

    <div id="pagination-wrapper">
        <?php global $wp_query; ?>

        <?php if ( $wp_query->max_num_pages > 1 ) : ?>
			<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'twentyeleven' ) ); ?></div>
			<div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'twentyeleven' ) ); ?></div>
        <?php endif; ?>

    </div>

    </div>

    <?php get_template_part('blog_sidebar'); ?>
</div>


<div id="upper-footer">
    <?php get_template_part('footer_collab'); ?>
    <?php get_template_part('footer_engineering'); ?>
    <?php get_template_part('footer_news'); ?>
</div>


<?php get_footer(); ?>
