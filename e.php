<?php

/**
 * @author erik@heythere.org
 */
class HeyEmail {

    /**
     * Send a notification email
     * 
     * @param string    $email
     * @return void
     */
    private function __email($data) {
        $__to = "kezra@cdcreative.com";
        $__cc = "erik@heythere.org";
        $__subject = "AG Contact Form Response";

        $header  = "From: {$data['e']}\r\n";
        $header .= "Reply-To: {$data['e']}\r\n";
        $header .= "X-Mailer: Hey There\r\n";
        $header .= "X-Sender-IP: {$_SERVER['REMOTE_ADDR']}\r\n";
        $header .= "Cc: " . $__cc . "\r\n";
        
        $email = "";

        foreach($_POST as $k => $p):
            $m = false;

            switch($k):
                case 'e':
                    $m = 'Email Address:';
                    break;
                case 'contact-phone':
                    $m = 'Phone Number:';
                    break;
                case 'contact-type':
                    $m = 'Type of Project:';
                    break;
                case 'contact-message':
                    $m = 'Message:';
                    break;
                case 'contact-company':
                    $m = 'Company Name:';
                    break;
                case 'contact-newsletter':
                    $m = 'Please subscribe me to your newsletter.';
                    $p = '';
                    break;
                case 'contact-satisfied':
                    $m = 'Are you satisfied with the service your community gets from your design professional?';
                    $p = $p ? 'Yes' : 'No';
                    break;
                case 'contact-care':
                    $m = 'Are the long term care environments in your community resident centered?';
                    $p = $p ? 'Yes' : 'No';
                    break;
                case 'contact-falling':
                    $m = 'Is your community falling behind the competition?';
                    $p = $p ? 'Yes' : 'No';
                    break;
                case 'contact-plan':
                    $m = 'Does your community have a master plan for the future?';
                    $p = $p ? 'Yes' : 'No';
                    break;
                case 'contact-budget':
                    $m = 'Was the latest construction project at your community delivered on budget?';
                    $p = $p ? 'Yes' : 'No';
                    break;

            endswitch;

            if(empty($p)):
                $p = 'Not provided';
            endif;

            if($m != false):
                $email .= $m . " " . $p . "\n\n"; 
            endif;

        endforeach;

        return mail($__to, $__subject, $email, $header); 
    }

    /**
     * Check for sane email addresses
     *
     * Email regex by Michael Rushton
     * http://fightingforalostcause.net/misc/2006/compare-email-regex.php
     *
     * @param string    $email
     * @return boolean 
     */
    private function __is_email($email) {
        $re = '/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-[a-z0-9]+)*\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-[a-z0-9]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/iD';

        return preg_match($re, $email) == true;
    }

    /**
     * Output message for ajax
     *
     * @param string    $msg
     * @param array     $classes
     * @return void
     */
    private function __message($msg, $classes = array()) {
        if(isset($_POST['ajax'])):
            $output = '<p class="';
        
            foreach($classes as $class):
                $output .= $class . ' ';
            endforeach;

            $output .= 'message">';
            $output .= $msg;
            $output .= '</p>';

            echo $output;

            die();
        endif;
    }

    /**
     * Point of entry - handles requests and assembles output
     *
     * @return void
     */
    public function dispatch() {
        if(!empty($_POST) && $_POST['email'] == "Email Address" && $_POST['name'] == ""):
            if(self::__is_email($_POST['e'])):
                // Send email

                if(!self::__email($_POST)):
                    self::__message("There was a problem sending the email.", array("error"));
                endif;
            else:
                self::__message("Please enter a valid email address", array("error"));
            endif;
            
            self::__message("Thank you. Your Email has been sent.");
        endif;
    }
}

HeyEmail::dispatch();
