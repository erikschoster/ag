<?php
/**
 * Template Name: Process 
 */
?>

<?php get_header(); ?>
<?php while(have_posts()): the_post(); ?>

<div id="breadcrumbs"></div>

<div id="custom-page-header-wrapper">
    <img src="/wp-content/themes/ag/img/process-header.jpg" />
    <h2 id="custom-page-header">Our Collaborative Process</h2>
</div>

<div class="engineering-wrapper" id="project-landing-content">
    <div id="our-work-content-wrapper">
        <script src="/wp-content/themes/ag/js/process.js"></script>
        <div class="our-work-content" id="engineering-content">
            <h3>Collaborative Process</h3>

            <p><strong>Collaborate: to work, one with another, to cooperate</strong></p>
            <p>You have a vision, a goal, an idea. It’s our task to listen, interpret, articulate a solution and build consensus. Together, working hand in hand with your development team, we will implement it. The AG collaborative process has been an integral component of our ability to build long-term relationships with our clients. </p>
        </div>

        <div id="process-thumbs">
            <a href="#" id="process-identify">Identify</a>
            <a href="#" id="process-listen">Listen</a>
            <a href="#" id="process-weave">Weave</a>
            <a href="#" id="process-build">Build</a>
            <a href="#" id="process-deliver">Deliver</a>
        </div> 

        <div id="process-info">
            <div id="process-identify-info">
                <p><em>Identify</em></p>
                <h5>To recognize, to establish</h5>
                <p>The architechural process begins by identifying the Right Fit. In a truly 
                    collaborative partnership this means that the relationship is the right fit for 
                    both parties. The client must feel confident the architectural firm will fulfill 
                    their goals and objectives and hopefully exceed expectations. 
                    The architectural firm depends on a client committed to every part of the design, 
                    development and construction process.
                </p>
                <p><strong><span><a href="/identify-the-right-fit-is-ag-the-right-architect-for-you/">Identify the Right Fit - Is AG the Right Architect for You?</a></span></strong></p>
            </div>
            <div id="process-listen-info">
                 <p><em>Listen</em></p>
                <h5>To make an effort to hear, to pay attention</h5>
                <p>AG has earned the reputation of being “good listeners.” We take the time to truly understand both your operational objectives and economic parameters before we weave our experience into the solution. And now in the new post recession environment, we are paying special attention to the creative ways our clients are fine tuning their products to suit the realities of the new economy. We are committed to building consensus around a solution that is appropriate for your project rather than trying to convince you that you must accept some current “trend.” The solution must be tailored to suit the context and nuances of your particular needs.
                </p>
            </div>
            <div id="process-weave-info">
                <p><em>Weave</em></p>
                <h5>To interlace, combine, introduce an element into a connected whole</h5>
                <p>After we listen and understand your project specific needs, we interpret and apply appropriate design principles into your solution. Whether senior living, long-term care, memory support, multi-family or mixed use, your solution will be enriched by our earlier experience and benefit from lessons learned and ideas explored. We stay tuned to the trends in your particular building type, and not just in theory, but in actual practice. We blend universally accepted design principles that are appropriate for your unique situation. 
                </p>

            </div>
            <div id="process-build-info">
                 <p><em>Build</em></p>
                <h5>To establish, to increase, to strengthen</h5>
                <p>Before we can build a project, we need to build consensus with your decision making team. Throughout the design process the table is open to all ideas. An effective creative exchange relies on this open dialogue. In order to keep a project on track, the time comes to narrow down the options and formulate a solution that best suits the client and the project objectives.
                </p>
                <p><strong><span><a href="/how-ag-builds-consensus/">Learn How AG Builds Consensus</a></span></strong></p>
            </div>
            <div id="process-deliver-info">
                 <p><em>Deliver</em></p>
                <h5>To give into another’s possession</h5>
                <p>AG has learned the importance of controlling construction costs during the design process in order to deliver your project on budget. We have pioneered a unique methodology that facilitates the development team’s ability to maintain and consistently evaluate the project budget. Through a series of articulated building sets produced during the Conceptual Design, Schematic Design and Design Development phases, the team is continuously informed on the status of the budget. Together, the team can make informed choices regarding construction costs so that at the end of the Design Development stage, all the heavy decisions have been made and a GMP can be established.
                </p>
                <p><strong><span><a href="/the-ag-method-of-delivery/">The AG Method of Delivery</a></span></strong></p>
            </div>
        </div>

    </div>
</div>

<div id="upper-footer">
    <?php get_template_part('footer_collab'); ?>
    <?php get_template_part('footer_engineering'); ?>
    <?php get_template_part('footer_news'); ?>
</div>

<?php endwhile; ?>
<?php get_footer(); ?>
