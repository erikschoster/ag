<?php get_header(); ?>
<?php while(have_posts()): the_post(); ?>

<div id="breadcrumbs"></div>

<div id="blog-content-wrapper">

    <h2 id="blog-recent-posts">Senior Associates</h2>

    <div id="associate-sidebar">
        <h3>Team Members</h3>
        <?php $associates = get_posts(array('post_type'=>'hey_associate', 'numberposts'=>-1)); ?>
        <ul>
            <li class="associate-outgoing"><a href="/leaders/eugene-r-guszkowski/">Meet the<br/> Leadership Team &gt;</a></li>
            <li class="associate-outgoing bottom"><a href="/supports/amber-bahr/">Meet the<br/> AG Team &gt;</a></li>
        <?php foreach($associates as $associate): ?>
            <?php $current_associate = ($associate->ID == get_the_ID()) ? 'orange' : 'white'; ?>
            <li><a class="<?php echo $current_associate; ?>" href="<?php echo get_permalink($associate->ID); ?>"><?php echo $associate->post_title; ?></a></li>
        <?php endforeach; ?>
        </ul>
    </div>

    <div id="blog-content" class="associate-content">
        <?php $meta = $ag->get_associate_meta(get_the_ID()); ?>

        <div id="associate-meta-left">

            <h3><?php the_title(); ?></h3>
            <p><?php echo $meta['job_title']; ?></p>
            <p><?php echo $meta['bio']; ?></p>

      
 
            <h4>Areas of Expertise:</h4>
            <p><?php echo $meta['expertise']; ?></p>
 
            <h4>Education:</h4>
            <p><?php echo $meta['education']; ?></p>
        </div>
 
        <div id="associate-meta-right">
            <?php $images = $ag->project_gallery(get_the_ID(), 'associate_thumb'); ?>
            <?php $image = array_shift($images); ?>
            <img src="<?php echo $image[0]; ?>" />
 
            <blockquote><?php echo get_post_meta(get_the_ID(), 'hey_associate_quote', true); ?>
            <p><em><?php echo get_post_meta(get_the_ID(), 'hey_associate_attribution', true); ?></em></p>
            </blockquote>


            <?php if(!empty($meta['project'])): ?>
            <div id="signature-project-wrapper">
            <div id="signature-project">
                <?php $images = $ag->project_gallery($meta['project'], 'medium'); ?>
                <?php $image = array_shift($images); ?>
                <img src="<?php echo $image[0]; ?>" />
            </div>
            <div id="signature-project-info">
                <p><strong>Signature Project:</strong></p>             
                <p><?php echo get_the_title($meta['project']); ?><br/>
                <?php echo get_post_meta($meta['project'], 'hey_project_location', true); ?></p>
            </div>
            </div>
            <?php endif; ?>

        </div>
    </div>

</div>

<div id="upper-footer">
    <?php get_template_part('footer_collab'); ?>
    <?php get_template_part('footer_experience'); ?>
    <?php get_template_part('footer_awards'); ?>
</div>

<?php endwhile; ?>
<?php get_footer(); ?>
