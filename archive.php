<?php
/**
 * Template Name: News 
 */
?>
<?php error_reporting(-1); ?>
<?php get_header(); ?>


<h2 id="blog-recent-posts">News</h2>

<div id="blog-content-wrapper">
    <div id="blog-content" class="news-landing">

    <?php $wp_query = new WP_Query('posts_per_page=3&paged='.$paged); ?>
    <?php while($wp_query->have_posts()): $wp_query->the_post(); ?>
    <div class="post-wrapper">
        <h3 class="post-header"><?php the_title(); ?></h3>
        <p class="post-leader"><?php the_date(); ?>, <?php the_tags('In ', ', ', ','); ?> by <?php the_author(); ?></p>

        <?php if(has_post_thumbnail()): ?>
        <div class="post-thumb">
            <?php echo get_the_post_thumbnail(get_the_ID(), 'medium'); ?>
        </div>
        <?php endif; ?>

        <?php $content = get_the_content(); ?>
        <?php $content = strip_tags($content, '<p><b><i><strong><em><div><li><ul>'); ?>
        <?php $content = apply_filters('the_content', $content); ?>
        <?php echo substr($content, 0, 500); ?>...
        <a href="<?php the_permalink(); ?>" class="post-readmore">Read more...</a>
    </div>
    <?php endwhile; ?>

    <div id="pagination-wrapper">
        <?php posts_nav_link(); ?>
    </div>

    </div>

    <?php get_template_part('blog_sidebar'); ?>
</div>


<div id="upper-footer">
    <?php get_template_part('footer_collab'); ?>
    <?php get_template_part('footer_engineering'); ?>
    <?php get_template_part('footer_news'); ?>
</div>


<?php get_footer(); ?>
