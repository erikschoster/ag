<?php get_header(); ?>
<?php while(have_posts()): the_post(); ?>

<div id="breadcrumbs"></div>

<div id="project-detail-content-wrapper">
    <?php $category = $ag->get_project_category(get_the_ID()); ?>
    <h2 id="landing-header">
        <?php echo $category['name']; ?>
    </h2>

    <div id="project-detail-body-wrapper">

        <?php $ag->project_nav($category['id']); ?>

        <div id="project-gallery-wrapper">
        <?php $images = $ag->project_gallery(get_the_ID(), 'project_image'); ?>
        <div id="project-gallery">
            <?php foreach($images as $image): ?>
                <img src="<?php echo $image[0]; ?>" ?>
            <?php endforeach; ?>
        </div>

        <?php $thumbs = $ag->project_gallery(get_the_ID(), 'project_thumb'); ?>
        <div id="project-gallery-nav-wrapper">
        <div id="project-gallery-nav">
            <a href="#" id="project-gallery-prev">previous image</a>
            <div id="project-gallery-nav-thumbs-wrapper">
            <ul id="project-gallery-nav-thumbs">
            <?php foreach($thumbs as $thumb): ?>
                <li><a href="#"><img src="<?php echo $thumb[0]; ?>" ?></a></li>
            <?php endforeach; ?>
            </ul>
            </div>
            <a href="#" id="project-gallery-next">next image</a>
        </div>
        </div>
        </div>
            

        <div class="project-detail-content" id="senior-living-content">
            <h3><?php the_title(); ?></h3>

            <?php $meta = $ag->get_project_meta(get_the_ID()); ?>

            <div id="project-details">
                <p><?php echo $meta['details']; ?></p>
            </div>

            <div id="project-description">
                <p><?php echo $meta['description']; ?></p>

                <?php if(!empty($meta['case_study'])): ?>
                <p><a href="<?php echo $meta['case_study']; ?>">Case Study &gt;&gt;</a></p>
                <?php endif; ?>
            </div>

            <div id="project-quote">
                <p><?php echo $meta['quote']; ?></p>
                <p><em><?php echo $meta['attribution']; ?></em></p>
            </div>
        </div>
    </div>
</div>

<div id="upper-footer">
    <?php get_template_part('footer_collab'); ?>
    <?php get_template_part('footer_experience'); ?>
    <?php get_template_part('footer_news'); ?>
</div>

<?php endwhile; ?>
<?php get_footer(); ?>
