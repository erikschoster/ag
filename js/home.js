jQuery(document).ready(function(){
        jQuery('#home-slideshow').cycle({
                fx: 'fade',
                speed: 1800,
                timeout: 4000,
                pager: '#home-slideshow-nav',
                pause: 1,
                pauseOnPagerHover: 1,
        });
});
