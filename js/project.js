jQuery(document).ready(function(){
        jQuery('#project-gallery').cycle({
                fx: 'fade',
                speed: 2000,
                timeout: 0,
                pause: 1,
                pauseOnPagerHover:1,
                next: '#project-gallery-next',
                prev: '#project-gallery-prev',
                pager: '#project-gallery-nav-thumbs',
                pagerAnchorBuilder: function(idx, slide) {
                    return '#project-gallery-nav-thumbs li:eq(' + idx + ') a';
                },
        });

        jQuery('#project-gallery-nav-thumbs li').mouseover(function(e) {
            var frameStart = jQuery('#project-gallery-nav-thumbs-wrapper').offset().left;
            var frameEnd = frameStart + jQuery('#project-gallery-nav-thumbs-wrapper').width();
            var thumbStart = jQuery(this).offset().left;
            var thumbEnd = thumbStart + jQuery(this).width();
            var thumbOffsetRight = frameEnd - thumbEnd;
            var thumbOffsetLeft = thumbStart - frameStart;

            if (thumbOffsetRight < 0) {
                jQuery(this).parent().animate({left: '-=80'}, 2000);
            } 
            
            if (thumbOffsetLeft < 0) {
                jQuery(this).parent().animate({left: '+=80'}, 2000);
            }
        });
});

