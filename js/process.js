jQuery(document).ready(function(){
    jQuery('#process-identify').css('background-position', '0px 155px'); 
    jQuery('#process-identify-info').fadeIn(400);

    jQuery('#process-thumbs a').click(function(e) {
        e.preventDefault();

        jQuery('#process-thumbs a').css('background-position', '0px 0px'); 

        jQuery(this).css('background-position', '0px 155px'); 

        var thumb_id = '#' + jQuery(this).attr('id') + '-info';
        jQuery('#process-info div').hide();

        jQuery(thumb_id).fadeIn(400);
    });
});
