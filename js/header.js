jQuery(document).ready(function(){
        jQuery('div#navigation li.subtrigger').hover(
            function(){
                jQuery(this).children('div.subnavigation').slideDown('fast');
            },

            function(){
                jQuery(this).children('div.subnavigation').slideUp('fast');
            }
        );

        jQuery('ul.nav li').hover(
            function(){
                jQuery(this).children('ul.subnav').slideDown('fast');
                jQuery(this).children('a').css('background-color', '#393a3c');
            },

            function(){
                jQuery(this).children('ul.subnav').slideUp('fast');
                jQuery(this).children('a').css('background-color', 'transparent');
            }
        );
});
