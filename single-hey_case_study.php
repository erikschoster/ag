<?php get_header(); ?>
<?php while(have_posts()): the_post(); ?>

<div id="breadcrumbs"></div>
<h2 id="blog-recent-posts"><?php the_title(); ?></h2>

<div id="blog-content-wrapper">
    <div id="blog-content" class="case-study-content">
        <?php $meta = $ag->get_case_study_meta(get_the_ID()); ?>

        <h3 id="case-study-header"><?php the_title(); ?></h3>
        <p><em><?php the_date(); ?>, In Case Studies, Recent Posts, by <?php the_author(); ?></em></p>

        <div id="case-study-meta">

        <h4>Location:</h4>
        <p><?php echo $meta['location']; ?></p>

        <h4>Owner:</h4>
        <p><?php echo $meta['owner']; ?></p>
 
        <h4>Project Challenge:</h4>
        <p><?php echo $meta['project_challenge']; ?></p>
 
        <h4>Design Solution:</h4>
        <p><?php echo $meta['design_solution']; ?></p>
  
        <h4>Date of Completion:</h4>
        <p><?php echo $meta['date']; ?></p>

        <h4>Details:</h4>
        <p><?php echo $meta['details']; ?></p>
        </div>

        <div id="case-study-photos">
            <?php $images = $ag->project_gallery(get_the_ID(), 'case_study_image'); ?>
            <?php foreach($images as $image): ?>
                <img src="<?php echo $image[0]; ?>" width="368px" height="215px" />
            <?php endforeach; ?>
        </div>

    </div>

    <?php get_template_part('blog_sidebar'); ?>
</div>

<div id="upper-footer">
    <?php get_template_part('footer_collab'); ?>
    <?php get_template_part('footer_experience'); ?>
    <?php get_template_part('footer_awards'); ?>
</div>

<?php endwhile; ?>
<?php get_footer(); ?>
