<div id="home-footer-news">
    <h3>Our News</h3>
    <div class="home-news-teaser">
        <?php $news_query = new WP_Query(array('post_type' => array('post'), 'posts_per_page' => 2)); ?>
        <?php while($news_query->have_posts()): $news_query->the_post(); ?>
        <div class="news-teaser-wrapper">
            <h4><?php the_title(); ?></h4>
            <p>
                <?php $content = get_the_content(); ?>
                <?php $content = strip_tags($content, '<p><b><i><strong><em><div><li><ul>'); ?>
                <?php $content = apply_filters('the_content', $content); ?>
                <?php echo substr($content, 0, 90); ?>...
                <a href="<?php the_permalink(); ?>">read more&gt;</a>
            </p>
        </div>
        <?php endwhile; ?>
    </div>
</div>
