<?php get_header(); ?>

<?php get_template_part('slider'); ?>

<div id="home-thumbnails">
    <div class="home-thumbnail" id="home-thumb-affordable"> 
        <a href="/our-work/affordable-housing/">
            <p>Affordable Housing &gt;</p>
            <img src="/wp-content/themes/ag/img/thumb-affordable.jpg" />
        </a>
    </div>

    <div class="home-thumbnail"> 
        <a href="/our-work/multi-family/">
            <p>Multi-Family &gt;</p>
            <img src="/wp-content/themes/ag/img/thumb-multi.jpg" />
        </a>
    </div>

     <div class="home-thumbnail"> 
        <a href="/our-work/mixed-use/">
            <p>Mixed Use / Retail &gt;</p>
            <img src="/wp-content/themes/ag/img/thumb-mixed.jpg" />
        </a>
    </div>

    <div class="home-thumbnail" id="home-thumb-student"> 
        <a href="/our-work/student-housing/">
            <p>Student Housing &gt;</p>
            <img src="/wp-content/themes/ag/img/thumb-student.jpg" />
        </a>
    </div>

</div>

<div id="upper-footer">
    <?php get_template_part('footer_collab'); ?>
    <?php get_template_part('footer_engineering'); ?>
    <?php get_template_part('footer_news'); ?>
</div>

<?php get_footer(); ?>
